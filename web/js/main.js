jQuery(document).ready(function ($) {
    $('.addColt').on('click', function () {
            $(".addBlock:eq(0)").clone().appendTo(".tbl tbody:last").css('display', 'table-row');
    })

    $('[data-id]').css('display', 'none');
    $('.edit').on('click', function (e) {
        var i = $(this).attr('data-my');
        $(this).css('display', 'none');
        var dataEdit = $('[data-edit='+i+']').css('display', 'block');
        if(dataEdit.css('display') == 'block'){
            $('[data-field='+i+']').css('display', 'none');
            $('[data-id='+i+']').css('display', 'block');
        }
        $('[data-id='+i+']').click(function () {
            $(this).html('<br/>Сохранено');
        })
    });

    $('.save').on('click', function (e) {
        var i = $(this).attr('data-id');
        var v = $('[data-input='+i+']').val();

        $.ajax({
            url: '/cashbox/edit',
            type: 'POST',
            data: {
                    "payment_sum": v,
                    "id": i
                  },

            success: function (res) {

            },
            error: function () {
                alert('Ошибка при обновлении сумми');
            }
        })
    })




});
