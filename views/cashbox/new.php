<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

    <table class="tbl">
        <thead>
        <tr>
            <th scope="col">ФИО</th>
            <th scope="col">ID</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($clients as $val): ?>
            <tr>
                <th><?= $val['client_snp'] ?></th>
                <th><?= $val['id'] ?></th>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br/>


<?php $form = ActiveForm::begin() ?>

<?= $form->field($model, 'client_id')->input('text') ?>

<?= $form->field($model, 'payment_sum')->input('text') ?>

<?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>
