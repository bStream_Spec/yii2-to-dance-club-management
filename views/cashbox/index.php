<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Clients;
?>
<?php $sum = 0; ?>
<?php $Allsum = 0; ?>
<?php foreach ($cashbox as $cash): ?>
    <?php $Allsum += $cash['begin_saldo']; ?>
<?php endforeach; ?>

<?php $form = ActiveForm::begin() ?>

<?= $form->field($cashbox_model, 'payment_day')->widget(DatePicker::classname(), [
    'language' => 'ru',
    'dateFormat' => 'php:Y/m/d',
]) ?>
    <?= $form->field($cashbox_model, 'administrator_id')->dropDownList(
            ArrayHelper::map(User::find()->all(),"id","login"),
        ['prompt'=>'Выберите автора'])
        ->label("Автор")
    ?>

<?= $form->field($cashbox_model, 'begin_saldo')->input('text') ?>

<br/>
<div class="btn btn-default addColt">Добавить строчку</div>

<table class="table table-responsive">
    <table class="tbl">
        <thead>
        <tr>
            <th scope="col">Клиент</th>
            <th scope="col">Сумма оплаты</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($money as $mon): ?>
            <tr>
                <th>
                    <?= $mon['clients']['client_snp']; ?>
                </th>
                <th>
                    <span class="sumField" data-field="<?= $mon['id'] ?>"><?= $mon['payment_sum']; ?></span>
                    <span class="sumFieldEdit" data-edit="<?= $mon['id'] ?>" ><?= $form->field($money_model,
                            'payment_sum')->textInput(['value' => $mon['payment_sum'], 'data-input' => $mon['id']])  ?></span>
                </th>
                <th>
                    <span class="edit glyphicon glyphicon-pencil" data-my="<?= $mon['id'] ?>"></span>
                        <span data-id="<?= $mon['id'] ?>" class="save glyphicon glyphicon-ok"></span>
                </th>
                <?php $sum += $mon['payment_sum']; ?>
            </tr>
        <?php endforeach; ?>
            <tr class="addBlock">
                <th><?= $form->field($money_model, 'client_id')->dropDownList(ArrayHelper::map(Clients::find()->
                    all(),'id','client_snp'),['value'=>$mon['clients']['client_snp']]);  ?></th>
                <th><?= $form->field($money_model, 'payment_sum')->textInput()  ?></th>
            </tr>
        </tbody>
    </table>
</table>
    <br/>
    <p>Итого поступлений <input type="text" value=" <?= $sum ?> "/></p>
    <p>Сумма на конец <input type="text" value=" <?= $Allsum + $sum ?> "/></p>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>
<br/>



</div>
