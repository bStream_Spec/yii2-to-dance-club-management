<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<?php if(!Yii::$app->user->isGuest): ?>

<div class="container">
    <a href="<?= \yii\helpers\Url::to(['cashbox/index']) ?>"><button class="btn btn-default">Добавить дневную касу</button></a>


<table class="table table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Дата</th>
            <th scope="col">Администратор</th>
            <th scope="col">Сума на начало</th>
            <th scope="col">Оборот</th>
            <th scope="col">Сумма на конец</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($cashbox as $val): ?>
            <?php $max = $val['begin_saldo'] + $sum ?>
        <tr>
            <th><?= $val['payment_day'] ?></th>
            <th><?= $val['user']['login'] ?></th>
            <th><?= $val['begin_saldo'] ?></th>
            <th><?= $Allsum - $val['begin_saldo'] ?></th>
            <th><?= $Allsum  ?></th>
        </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
</table>

</div>

<?php endif; ?>
<?php if(Yii::$app->user->isGuest) : ?>
<h2>Вход</h2>
<p>Login: vadim</p>
<p>Password: 1111</p>
<p></p>
<?php $form =  ActiveForm::begin() ?>

<?= $form->field($login_model, 'login')->textInput(['autofocus' => true]) ?>
<?= $form->field($login_model, 'password')->input('password') ?>

<?= Html::submitButton('Вход', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>

<?php endif; ?>

