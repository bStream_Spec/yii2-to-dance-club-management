<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;


?>
<h2>Регистрация</h2>

<?php $form =  ActiveForm::begin() ?>

<?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'password')->input('password') ?>

<?= Html::submitButton('Регистрация', ['class' => 'btn btn-success']) ?>

<?php ActiveForm::end() ?>

