<?php

namespace app\controllers;

use app\models\Clients;
use app\models\Money;
use yii\web\Controller;
use app\models\Cashbox;

use Yii;
class CashboxController extends Controller{

    public function actionIndex(){

        $cashbox_model = new Cashbox();
        $money_model = new Money();

        $clients = Clients::find()->asArray()->with('money')->all();
        $money = Money::find()->asArray()->with('clients')->all();
        $cashbox = Cashbox::find()->select('begin_saldo')->asArray()->all();

        if( $cashbox_model->load(Yii::$app->request->post()) && $money_model->load(Yii::$app->request->post())){

            if($cashbox_model->save() && $money_model->save()){

                $money_model->client_id = Yii::$app->request->post()->client_id;
                $money_model->payment_sum = Yii::$app->request->post()->payment_sum;
                Yii::$app->session->setFlash('success', 'Добавлено');
                return $this->redirect(Yii::$app->request->referrer);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при добавление');
            }
        }

        return $this->render('index', compact('cashbox_model', 'money_model', 'clients', 'money', 'cashbox'));
    }

    public function actionEdit(){


        $id = Yii::$app->request->post('id');
        $money = Money::findOne($id);

        $money->payment_sum = Yii::$app->request->post('payment_sum');
        $money->save();

    }

}