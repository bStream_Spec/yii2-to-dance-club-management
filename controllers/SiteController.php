<?php

namespace app\controllers;

use app\models\Cashbox;
use app\models\Clients;
use Yii;
use app\models\Signup;
use app\models\Login;
use yii\web\Controller;
use app\models\Money;


class SiteController extends Controller{

    public function actionIndex(){
        $cashbox = Cashbox::find()->asArray()->with('user')->all();
        $money = Money::find()->asArray()->with('clients')->all();
        $clients = Clients::find()->asArray()->with('money')->all();
        $login_model = new Login();

        $Allsum = 0;
        $sum = 0;
        foreach ($cashbox as $cash){
            $Allsum += $cash['begin_saldo'];
        }
        foreach ($money as $mon) {
            $sum += $mon['payment_sum'];
        }


        if(Yii::$app->request->post('Login')){
            $login_model->attributes = Yii::$app->request->post('Login');

            if($login_model->validate()){
                Yii::$app->user->login($login_model->getUser());
                return $this->goHome();
            }
        }

        return $this->render('index', compact('cashbox', 'login_model', 'clients' , 'Allsum', 'sum'));
    }

    public function actionLogout(){
        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
            return $this->goHome();
        }
    }

    public function actionSignup(){

        $model = new Signup();
        if(isset($_POST['Signup'])){
            $model->attributes = Yii::$app->request->post('Signup');

            if($model->validate() && $model->signup()){
                return $this->goHome();
            }
        }

        return $this->render('signup', compact('model'));
    }



}
