<?php

namespace app\models;

use yii\db\ActiveRecord;

class Cashbox extends ActiveRecord{

    public static function tableName()
    {
        return 'cashbox';
    }

    public function rules()
    {
        return [
            [['payment_day', 'administrator_id', 'begin_saldo'], 'required'],
            ['begin_saldo', 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_day' => 'Дата',
            'administrator_id' => 'Администратор',
            'begin_saldo' => 'Сумма на начало',
        ];
    }

    public function getUser(){
        return $this->hasOne(User::classname(), ['id' => 'administrator_id']);
    }



}