<?php

namespace app\models;

use yii\base\Model;

class Login extends Model{

    public $login;
    public $password;

    public function rules(){
        return [
            [ ['login', 'password'], 'required' ],
            [ ['login', 'password'], 'trim' ],
            ['password', 'validatePassword']
        ];
    }

    public function validatePassword($attribute, $params){
        $user = $this->getUser();
        if(!$user || ($user->password != $this->password)){
            $this->addError($attribute, 'Логин или пароль введены неверно');
        }
    }

    public function getUser(){
        return User::findOne(['login' => $this->login]);
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
        ];
    }

}