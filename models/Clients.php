<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $client_snp
 * @property string $client_phone
 * @property string $created_at
 * @property string $updated_at
 */
class Clients extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['client_snp', 'client_phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_snp' => 'ФИО',
            'client_phone' => 'Телефон клиента',
            'created_at' => 'Дата и время создания',
            'updated_at' => 'Дата и время редактирования',
        ];
    }

    public function getMoney(){
        return $this->hasOne(Money::classname(), ['client_id' => 'id']);
    }
}
