<?php

namespace app\models;

use yii\db\ActiveRecord;

class Money extends ActiveRecord{

    public static function tableName()
    {
        return 'money';
    }

    public function rules()
    {
        return [
            [['client_id', 'payment_sum'], 'safe'],
            ['client_id', 'integer'],
            ['payment_sum', 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id' => '',
            'payment_sum' => '',
        ];
    }

    public function getClients(){
        return $this->hasOne(Clients::classname(), ['id' => 'client_id']);
    }



}