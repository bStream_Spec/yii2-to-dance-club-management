<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {

    public static function tableName(){
        return 'users';
    }

    public static function findIdentity($id){
        return self::findOne($id);
    }


    public function getId(){
        return $this->id;
    }

    public function getCashbox(){
        return $this->hasOne(Cashbox::classname(), ['administrator_id' => 'id']);
    }

    public static function findIdentityByAccessToken($token, $type = null){
    }

    public function getAuthKey(){
    }

    public function validateAuthKey($authKey){
    }

}
