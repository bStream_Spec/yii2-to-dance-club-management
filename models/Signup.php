<?php

namespace app\models;

use yii\base\Model;

class Signup extends Model{

    public $login;
    public $password;

    public function rules(){
      return [
        [ ['login', 'password'], 'required' ],
        [ ['login', 'password'], 'trim' ],
        [ 'login', 'unique', 'targetClass' => 'app\models\User'],
      ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    public function signup(){
        $user = new User();
        $user->login = $this->login;
        $user->password = $this->password;
        return $user->save();
    }

}