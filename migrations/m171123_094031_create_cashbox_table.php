<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cashbox`.
 */
class m171123_094031_create_cashbox_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cashbox', [
            'id' => $this->primaryKey(),
            'payment_day' => $this->dateTime(),
            'administrator_id' => $this->integer(),
            'begin_saldo' => $this->float(),
            'created_at' => $this->dateTime()->defaultValue('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cashbox');
    }
}
