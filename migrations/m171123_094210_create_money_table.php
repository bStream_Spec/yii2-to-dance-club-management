<?php

use yii\db\Migration;

/**
 * Handles the creation of table `money`.
 */
class m171123_094210_create_money_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('money', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'payment_sum' => $this->float(),
            'created_at' => $this->dateTime()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->dateTime()->defaultValue(CURRENT_TIMESTAMP),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('money');
    }
}
