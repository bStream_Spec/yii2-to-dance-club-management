<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m171123_093531_create_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'client_snp' => $this->string(),
            'client_phone' => $this->string(),
            'created_at' => $this->dateTime()->defaultValue('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('clients');
    }
}
